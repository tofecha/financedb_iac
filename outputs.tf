# output "db_host" {
#   value = aws_db_instance.main.address
# }

# output "ec2_fdb_host" {
#   value = aws_instance.fdb.public_dns
# }

output "ec2_fdb_priv_ip_addr" {
  value       = aws_instance.fdb.*.private_ip
  description = "The private IP address of the main server instance."
}

output "ec2_fdb_pub_ip_addr" {
  value = aws_instance.fdb.*.public_ip
}

