# iam role policy
resource "aws_iam_role" "fdb" {
  name               = "${local.prefix}-fdb"
  assume_role_policy = file("./templates/fdb/instance-profile-policy.json")

  tags = local.common_tags
}

resource "aws_iam_role_policy_attachment" "fdb_attach_policy" {
  role       = aws_iam_role.fdb.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_instance_profile" "fdb" {
  name = "${local.prefix}-fdb-instance-profile"
  role = aws_iam_role.fdb.name
}

# generate inventory file for ansible
resource "local_file" "hosts_cfg" {
  content = templatefile("${path.module}/templates/ansible/hosts.tpl",
    {
      fdb_public_ip  = aws_instance.fdb.*.public_ip
      fdb_private_ip = aws_instance.fdb.*.private_ip
    }
  )
  filename = "${path.module}/hosts"
}

#  ec2 instance
resource "aws_instance" "fdb" {
  ami           = var.ec2_image
  instance_type = var.ec2_instance_type
  # user_data = file("./templates/fdb/user-data.sh")
  iam_instance_profile = aws_iam_instance_profile.fdb.name
  key_name             = var.ec2_keypair
  count                = var.ec2_count
  subnet_id            = aws_subnet.public_a.id

  vpc_security_group_ids = [
    aws_security_group.fdb.id
  ]

  # # root block storage
  # root_block_device {
  #   volume_size = "16"
  #   volume_type = "gp2"
  # }

  # # ebs block storage
  # ebs_block_device {
  #   device_name = "/dev/sdb"
  #   volume_size = "128"
  #   volume_type = "gp2"
  #   delete
  # }

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-fdb")
  )
}

# security groups
resource "aws_security_group" "fdb" {
  description = "Control fdb inbound and outbound access"
  name        = "${local.prefix}-fdb"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.common_tags
}

