# #Create S3 Bucket for remote state :
# resource "aws_s3_bucket" "nmsapps-fdb-s3-tfstate" {
#     bucket = "nmsapps-fdb-s3-tfstate"
#     versioning {
#         enabled = true
#     }

#     lifecycle {
#         prevent_destroy = true
#     }

#     tags {
#         Name = "s3 remote store"
#     }
# }

# #Create Dynamodb table for remote state locking :
# resource "aws_dynamodb_table" "nmsapps-fdb-tfstate-lock" {
#   name = "nmsapps-fdb-tfstate-lock"
#   hash_key = "LockID"
#   read_capacity = 5
#   write_capacity = 5

#   attribute {
#     name = "LockID"
#   type = "S"
#   }
# }

terraform {
  backend "s3" {
    bucket         = "nmsapps-fdb-s3-tfstate"
    key            = "nmsapps-fdb-key.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "nmsapps-fdb-tfstate-lock"
  }
}

provider "aws" {
  region  = var.ec2_region
  profile = var.profile
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManageBy    = "Terraform"
  }
}

data "aws_region" "current" {}
