variable "ec2_region" {
  default = "us-east-1"
}

variable "ec2_image" {
  #default = "ami-02e136e904f3da870" #amazonlinux2
  default = "ami-09e67e426f25ce0d7" #ubuntu20.04
}

variable "ec2_instance_type" {
  default = "t3.small"
}

variable "ec2_keypair" {
  default = "financedb-ec2-kp"
}

# variable "bastion_key_name" {
#   default = "bastion-ec2-kp"
# }

variable "ec2_count" {
  default = "1"
}

variable "profile" {
  default = "nmsapps-devops-ci"
}

variable "prefix" {
  default = "fdb"
}

variable "project" {
  default = "fdb"
}

variable "contact" {
  default = "tyrel@nms.ph"
}

# variable "db_username" {
#   description = "Username for the RDS mariadb instance"
# }

# variable "db_password" {
#   description = "Password for the RDS mariadb instance"
# }


